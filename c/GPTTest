/* Copyright 2016 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include        "PMInclude.h"
#include        "GPThdr.h"
#include        "FormatDef.h"
#include        "FormatBits.h"
#include        "Format.h"
#include        "PMModule.h"
#include        "scsisvce.h"
#include        "Diagnostic.h"

#define task_name "gpttst"          /* the name of our task */
static _kernel_swi_regs reg;
#define MaxPartitions 7
#define NumParts 2

int main (int argc,char* argv[])
{
  int i;
  int partn[MaxPartitions];
  int partcount=NumParts;
  if(argc<2)
  {
    printf("Drive ID not given\n");
    return 0;
  }
  int ID=atoi(argv[1]);

  if(CheckBootDiscID(ID,0,true))
  {
    printf("Target drive contains the boot disc - aborting\n");
    return 0;
  }

  if(argc==3)
  {
    int param=atoi(argv[2]);
    if ((param)&&(param<=MaxPartitions))
      {
        partcount=param;
      }
      else
      {
        printf("Either *gpttest with no parameters to get 4 equal partitions\n or *gpttest <n> where n is between 1 and %d\n",MaxPartitions);
        return NULL;
      }
  }
  FormatParams  FPs[MaxPartitions];
  FormatParamsp FPp = FPs;

  testfileinit ();
  BufSecInit();
  int capacity, sectorsize;
   ReadDiscCapacity(ID, OverrideKey ,&capacity, &sectorsize);
   capacity-=8; // compensate for what is used by partition tables etc
   partcount=(partcount<MaxPartitions)?partcount:MaxPartitions;
   int partsize = capacity/partcount;
   for(i=0;i<partcount;i++)
   {
     partn[i]=partsize;
   }
   printf("Formatting SCSI ID %x with %d partitions size %x\n",ID,partcount,partsize);
   // initialise what we want to do
   FPp->partition=0;
//   FPp->type=GUIDWinNorm;
//   strncpy(FPp->discname,"Windows1",FPNameLenMAX);
   FPp->type=GUIDFilecore;
   strncpy(FPp->discname,"Filecore1",FPNameLenMAX);
   FPp++;
   if(partcount>1)
   {
   FPp->partition=1;
   FPp->type=GUIDFilecore;
   strncpy(FPp->discname,"Filecore2",FPNameLenMAX);
   }
   if(partcount>2)
   {
   FPp++;
   FPp->type=GUIDFilecore;
   FPp->partition=2;
   strncpy(FPp->discname,"Filecore3",FPNameLenMAX);
   }
   if(partcount>3)
   {
   FPp++;
//   FPp->type=GUIDWinNorm;
//   strncpy(FPp->discname,"Windows1",FPNameLenMAX);
   FPp->type=GUIDFilecore;
   FPp->partition=3;
   strncpy(FPp->discname,"Filecore4",FPNameLenMAX);
   }
   if(partcount>4)
   {
   FPp++;
   FPp->type=GUIDFilecore;
   FPp->partition=4;
   strncpy(FPp->discname,"Filecore5",FPNameLenMAX);
   }
   if(partcount>5)
   {
   FPp++;
   FPp->type=GUIDFilecore;
   FPp->partition=5;
   strncpy(FPp->discname,"Filecore6",FPNameLenMAX);
   }
   if(partcount>6)
   {
   FPp++;
   FPp->type=GUIDFilecore;
   FPp->partition=6;
   strncpy(FPp->discname,"Filecore7",FPNameLenMAX);
   }

   //_swix(OS_Module,_INR(0,1) ,3, "SCSIFS");
    _swix(OS_ServiceCall,_INR(0,2),ID,Service_SCSIDetached,0x7);

 int result;
 result=PartitionDisc(ID, MaxPartitions, partcount, partn);
 testprint("result = %d\n",result);

 if(CheckGPTValid(ID))
 {
   ReadGPTInfo(ID);
   FPp = FPs;
   for (i=0; i<partcount;i++)
   {
     FormatPartition( ID, FPp++,sectorsize);
   }
   freepointers();
 }
 else
 {
   testprint(" CheckGPTValid returned false\n");
 }


   _swix(OS_ServiceCall,_INR(0,2),ID,Service_SCSIAttached,0x7);
printf("Completed\n");
}

void wimp_error(_kernel_oserror *er)
{
  /*
   * Generic routine to handle error reporting
   * through the wimp
   */

  if(er!=(_kernel_oserror *)NULL)
  {
  testprint("WE:%x %s\n",er->errnum,(int)(er->errmess));
   reg.r[0] = (int) er;
   reg.r[1] =       0;
   reg.r[2] = (int) task_name;
   _kernel_swi(Wimp_ReportError, &reg, &reg);
  }
}
